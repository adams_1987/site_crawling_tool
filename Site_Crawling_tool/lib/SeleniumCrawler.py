from selenium import webdriver
#from selenium.webdriver.firefox.options import Options
#from selenium.webdriver.chrome.options import Options
import urllib
import re
import time
from datetime import datetime
from bs4 import BeautifulSoup
from urllib.parse import urldefrag, urljoin
import distutils.dir_util
import json
from browsermobproxy import Server # https://browsermob-proxy-py.readthedocs.io/en/stable/
from lib.logger import *
from lib.HTMLReporting import *
import sys
from distutils.util import strtobool



class SeleniumCrawler(object):


    def __init__(self, config):
        
        self.vars= config

        #logging POC
        self.Crawlerlog = get_logger("Crawler",str(config.get('main_config','loglvl')),'Crawler.log')
        #self.Crawlerlog.debug('using python version: %s',sys.version_info[0])


        starttimestamp = time.time()

        # setting up variables
        self.base_url = str(self.vars.get('Test_Config','base_url'))
        self.testname = datetime.utcfromtimestamp(starttimestamp).strftime("%H%M%S%d%m%y") #generating folder name for current test
        self.crawled_urls = []
        self.url_queue = []
        #self.results = {}
        self.results = []
        self.url_error_list = {} #dictionary containing errors "url" : "error message"
        self.Capturatrafic = bool(strtobool(self.vars.get('Test_Config','Capturatrafic')))
        self.CaptureScreen = bool(strtobool(self.vars.get('Test_Config','CaptureScreen')))
        self.CaptureHeader = bool(strtobool(self.vars.get('proxy_config','CaptureHeader')))

        #add exclusion/inclusion lists Note that they don't need to be urls they can be any url fragment that will be checked against
        exclusion_list = str(self.vars.get('Test_Config','exclusion_list'))
        exclusion_list = exclusion_list.split(',')
        assert isinstance(exclusion_list, list) #Exclusion list - needs to be a list
        self.exludeURL = exclusion_list 
        while '' in self.exludeURL: self.exludeURL.remove('')  # need to ensure there are no empty elements in list

        inclusion_list = str(self.vars.get('Test_Config','inclusion_list'))
        inclusion_list = inclusion_list.split(',')        
        assert isinstance(inclusion_list, list) #inclusion list - needs to be a list
        self.includeURL = inclusion_list
        while '' in self.includeURL: self.includeURL.remove('')  # need to ensure there are no empty elements in list

        self.ResultDir = self.CreateResultDir(self.base_url, self.testname)




        #starting proxy 
        #TODO export to seperate function
        if self.Capturatrafic == True:
            proxyserverdict={'port': int(self.vars.get('proxy_config','proxy_port'))} #TODO move this to config along with browsermob location
            self.server = Server(str(self.vars.get('proxy_config','proxy_dir')),options=proxyserverdict)
            #self.server = Server(str(self.vars.get('proxy_config','proxy_dir')),{'port':1410})
            self.server.start()
            self.proxy = self.server.create_proxy()


        self.browser = self.SetupBrowser()


        self.testsummary={
            'testname' : self.testname,
            'starttime': datetime.utcfromtimestamp(starttimestamp).strftime("%H:%M:%S:%d.%m.%y/"),
            'starttimestamp': starttimestamp,
            'base_url': self.base_url,
            'resultdir': self.ResultDir,
            'Capturatrafic': self.Capturatrafic,
            'CaptureScreenshot': self.CaptureScreen,
            'crawledPages': 0,
            'errors': 0,
            'timeelapsed': ''
            }

        self.run_crawler()

#####################
## 
## create results directory
## generate initial result dictonary that will contain
##	- results directory
##	- start date
##	- base url
##
#####################


    def SetupBrowser(self):

        browser_width  = self.vars.get('browser_config','browser_width')
        browser_height = self.vars.get('browser_config','browser_height')

        try:
            if self.vars.get('Test_Config','testbrowser') == 'phantomjs':
                if self.Capturatrafic == True:
                    proxy_address = "--proxy=127.0.0.1:%s" % self.proxy.port
                    service_args = [ proxy_address, '--ignore-ssl-errors=yes','--web-security=no' ]
                else: service_args = []

                browser = webdriver.PhantomJS(service_args=service_args)

        
            elif self.vars.get('Test_Config','testbrowser') == 'chrome':

                chrome_exec=self.vars.get('browser_config','ch_exec')


                options = webdriver.chrome.options.Options()
                options.add_argument("--headless") # Runs Chrome in headless mode.
                options.add_argument('--no-sandbox') # Bypass OS security model
                options.add_argument('--disable-gpu')  # applicable to windows os only
                options.add_argument('start-maximized') # 
                options.add_argument('disable-infobars')
                options.add_argument("--disable-extensions")
                options.add_argument("--ignore-certificate-errors=True")

                
                options.add_argument("--disable-application-cache")
                options.add_argument("--disk-cache-size=0")

                if self.Capturatrafic == True:
                    #KNOWN ISSUE WITH SSL PROXY USING CHROME
                    
                    PROXY = '--proxy-server=http://127.0.0.1:'+str(self.proxy.port)
                    print(PROXY)
                    options.add_argument(PROXY)
                    



                browser = webdriver.Chrome(chrome_exec, chrome_options=options)
                 

            elif self.vars.get('Test_Config','testbrowser') == 'firefox':

                profile = webdriver.FirefoxProfile()
                                
                options = webdriver.firefox.options.Options()
                options.add_argument("--headless")


                if self.Capturatrafic == True:
                    #set FF preferences for more info visit: https://developer.mozilla.org/en-US/docs/Mozilla/Preferences/Mozilla_networking_preferences

                    profile.set_preference("network.proxy.type", 1)
                    profile.set_preference("network.proxy.http", '127.0.0.1' )
                    profile.set_preference("network.proxy.http_port", int(self.proxy.port))

                    profile.set_preference("network.proxy.ssl", '127.0.0.1' )
                    profile.set_preference("network.proxy.ssl_port", int(self.proxy.port))

                    profile.set_preference("browser.cache.disk.enable", False)
                    profile.set_preference("browser.cache.memory.enable", False)
                    profile.set_preference("browser.cache.offline.enable", False)
                    profile.set_preference("network.http.use-cache", False) 
                    profile.set_preference("browser.cache.disk_cache_ssl", False) 

                    profile.update_preferences()


                browser = webdriver.Firefox(firefox_profile=profile, options=options, executable_path=self.vars.get('browser_config','ff_geckolocation'))
                #browser = webdriver.Firefox(firefox_options=options, executable_path=self.vars.get('browser_config','ff_geckolocation'))
                #browser = webdriver.Firefox(firefox_options=options,  executable_path=self.vars.get('browser_config','ff_geckolocation')
                

            browser.set_window_size(browser_width,browser_height) #set browser window size
            self.Crawlerlog.info('using %s browser', self.vars.get('Test_Config','testbrowser'))

        except Exception as e:
            self.Crawlerlog.error('error while trying to initialise selenium browser browser type: %s, check configuration!! error message: %s ',self.vars.get('Test_Config','testbrowser'),e)

        return browser


    def CreateResultDir(self, base_url, testname):


        
        resultDIR = 'results' #main results folder
        

        #striping unwanted elements like protocol and anything after last /
        str_url = str(re.sub(r'(^https?://|www.)',"",base_url))
        str_url = self.strip_url(str_url)
        #TODO add filter for anything after last /


        path_split = str_url.split('/')                             # split to get path in list form
        domain_split = path_split[0]                                # assigning domain to own value
        del path_split[0]                                           # removing doain from path list
        domain_split = domain_split.split('.')                      # spliting domain
        domain_split.reverse()                                      # reverse domain order


        pathlist = domain_split + path_split                        # joining both lists
        pathlist.append(testname)                                   # adding testname at the end of list
        pathlist = filter(None, pathlist)                           # removing empty elements ('/' at the end can create empty element)

        for line in pathlist: resultDIR = resultDIR + '/' + line    # changing list to string that can be sued to create directory

        resultDIR = resultDIR + '/'
        ScreenshotDIR = resultDIR + 'Scrshot/'
        hardir = resultDIR + 'HAR/'

        distutils.dir_util.mkpath(resultDIR)                       #create results folder
        if self.CaptureScreen == True: distutils.dir_util.mkpath(ScreenshotDIR) #create screenshot folder
        if self.Capturatrafic == True: distutils.dir_util.mkpath(hardir) #create HAR folder

        return resultDIR
        


    def strip_url(self,url): #strip url from protocol & any unwanted things like # at the end. for easier comparing.
        url = str(re.sub(r'#.*',"",url))
        url = url.replace(' ','')
        url = url.replace('\r','')
        url = url.replace('\n','')
        url = url.replace('\t','')
        url.lower()

        return url


    def url_http_status(self,url): #checking for http status

        error  = None
        content_type = None
        try:
            header = urllib.request.urlopen(url)    #open url using urllib
            info = header.info()                    #parse header info
            content_type = info.get_content_type()  #get content_type
        except urllib.error.HTTPError as e:
            error = str(e.getcode)
            self.Crawlerlog.debug('test %s encountered HTTPError while checking: %s HTTPstatus. error message: %s',self.testname,url, str(e))
        except urllib.error.URLError as e:
            error = str(e.reason)
            self.Crawlerlog.debug('test %s encountered URLError while checking: %s HTTPstatus. error message: %s',self.testname,url, str(e))
        except Exception as e:
            self.Crawlerlog.warning('test %s encountered UNKNOWN error while checking: %s HTTPstatus. error message: %s',self.testname,url, str(e))
            error = str(e)


        return content_type, error


    def SeleniumLoadPage(self,url):

        

        try:

            StepID= str(len(self.crawled_urls)) #first we generate step id for current page
            harfile = ''                        #empty har file as we don't always will generate har (parameter setting)
            scrnshotfile = ''                   #empty har file as we don't always will generate screenshot (parameter setting)
            httpstatus = {}

            if self.CaptureHeader == True: 
                proxyOptions = {'captureHeaders': True}
            else: 
                proxyOptions = {}


            if self.Capturatrafic == True: self.proxy.new_har(StepID,options=proxyOptions)  #if we generate har start new proxy

            SelElapsed = time.time()

            self.browser.get(url)               # loading page using selenium

            SelElapsed = time.time() - SelElapsed 

            page_source = self.browser.page_source

            if self.CaptureScreen == True:      #if we chose to generate screenshot file now it's time to do so and populate empty scrnshotfile variable with results file location
                scrnshotfile = 'Scrshot/' + StepID + '.png'
                
                #get document dimensions for sake of consistancy between tests we set width to be parameter from config (unless changed during page load)
                height = self.browser.execute_script("return document.body.scrollHeight") 
                width = self.browser.get_window_size()
                
                #now we set browser size to match current document height in order to capture fullpage screenshot
                self.browser.set_window_size(width['width'],height)

                #take screenshot
                self.browser.save_screenshot(self.ResultDir+scrnshotfile)

                #IMPORTANT thing is to reset browser size after screenshot. without it we will render page for previous screen size which may cause page to get broken
                self.browser.set_window_size(self.vars.get('browser_config','browser_width'),self.vars.get('browser_config','browser_height'))

                self.Crawlerlog.debug('screenshot taken for url: %s height: %s width: %s filename: %s',url,height,width['width'],scrnshotfile)


                #browser_width  = self.vars.get('browser_config','browser_width')
                #browser_height = self.vars.get('browser_config','browser_height')



            if self.Capturatrafic == True:      #if we chose to generate har file now it's time to do so and populate empty harfile variable with results file location
                

                httpstatus = self.parseHarHTTPstatus(self.proxy.har['log']['entries'])

                harfile= 'HAR/' + StepID + '.har'
                h = open(self.ResultDir+harfile,'w')
                h.write(json.dumps(self.proxy.har))
                h.close()            

            # generating Dict with results
            CurrentResults ={
                'StepID': StepID, 
                'Url': url,
                'PageTitle': self.browser.title,
                'ElapsedTime': int(SelElapsed),
                'HarFileLocation': harfile,
                'ScreenshotLocation': scrnshotfile,
                'Networkrequests': httpstatus,
                }


        except Exception as e:
            self.Crawlerlog.warning('test %s encountered UNKNOWN error while loading Page using selenium. url: %s error message: %s',self.testname,url, str(e))
            self.ApendErrorList(url,e)
            page_source = None
            CurrentResults = {}

        return page_source, CurrentResults          

    def parseHarHTTPstatus(self, har):

        HTTPstatus = {'CodesSummary':{'1xx': [],
                                      '2xx': [],
                                      '3xx': [],
                                      '4xx': [],
                                      '5xx': [],
                                      'other':[],
                                      },
            'RequestsByStatusCodes': {'1xx': [],
                                      '2xx': [],
                                      '3xx': [],
                                      '4xx': [],
                                      '5xx': [],
                                      'other':[],
                                      },}
        for i in har:
            if 100 <= i['response']['status'] < 200: HTTPstatus = self.AddHttpStatusToList(i,HTTPstatus,'1xx')
            elif 200 <= i['response']['status'] < 300: HTTPstatus = self.AddHttpStatusToList(i,HTTPstatus,'2xx')
            elif 300 <= i['response']['status'] < 400: HTTPstatus = self.AddHttpStatusToList(i,HTTPstatus,'3xx')
            elif 400 <= i['response']['status'] < 500: HTTPstatus = self.AddHttpStatusToList(i,HTTPstatus,'4xx')
            elif 500 <= i['response']['status'] < 600: HTTPstatus = self.AddHttpStatusToList(i,HTTPstatus,'5xx')
            else: self.AddHttpStatusToList(i,HTTPstatus,'other')

        HTTPstatus = self.GenerateHarSum(HTTPstatus)

        return HTTPstatus

    def AddHttpStatusToList(self,i,HTTPstatus,coderange):
        HTTPstatus['RequestsByStatusCodes'][coderange].append({'url':   i['request']['url'],
                                                               'method':i['request']['method'],
                                                               'status':i['response']['status'],
                                                               'text':  i['response']['statusText'],
                                                              })
        return HTTPstatus 

    def GenerateHarSum(self,HTTPstatus):
        for k,v in HTTPstatus['RequestsByStatusCodes'].items():
            HTTPstatus['CodesSummary'][k] = len(HTTPstatus['RequestsByStatusCodes'][k])
        return HTTPstatus

    #append item to list of url that throw error during http check // selenium operations or generating soup maybe we will add additional steps here in future
    def ApendErrorList(self,url,error): 
        self.url_error_list[url]= error 
        self.Crawlerlog.debug('test %s appending errorlist dict. url: %s error message: %s',self.testname,url, error)



    def get_soup(self, html): #generating soup from page source
        if html is not None:
            soup = BeautifulSoup(html, 'lxml')
            return soup
        else:
            return

    def get_links(self, soup,CurrentResults): #getting urls using beautifulsoup then populating url_queue and gathering summary info

        urllist =[]

        for link in soup.find_all('a', href=True): #for loop looking for every href element found using beautifulsoup
            
            link = link['href'] #assigning href value to variable
            link = self.strip_url(link) #striping url from unwanted things like space page fragments (#) etc.
            
            url = urljoin(CurrentResults['Url'], urldefrag(link)[0]) #beautifulsoup can't handle relative links so we need to merge it with url from where it was taken (because we use includes using base_url is not best approach)

            if any(e in link for e in self.exludeURL): #if link is in exclusion list we simply skip it
                continue

            #time to check if current url should be added to urllist
            if url not in urllist:  #first we get rid of duplicates
                if url.startswith(self.base_url): #second we check if url match base_url
                    urllist.append(url)
                elif any(e in url for e in self.includeURL): #if url don't match base_url maybe it's in inclusionlist??
                    urllist.append(url)

        for u in urllist: #now it's time to populate urllist queue TODO: maybe we should move this to seperate method?
        
            if u not in self.url_queue and u not in self.crawled_urls: #if url is not in queue already or crawled already we can add it to queue
                self.url_queue.append(u)

        CurrentResults['ChildUrls']=urllist #let's add all childurl's to Summary

        
        return CurrentResults


    def run_crawler(self):

        self.Crawlerlog.info('test %s starting: baseurl: %s Capturatrafic: %s CaptureScreen: %s exclusion: %s inclusion: %s', str(self.testname), str(self.base_url), str(self.Capturatrafic), str(self.CaptureScreen), str(self.exludeURL), str(self.includeURL))

        testelapsedtime=time.time() #TMP

        self.url_queue.append(self.base_url)


        while len(self.url_queue): #If we have URLs to crawl - we crawl

            PageElapsedTime=time.time() #TMP
            
            current_url = self.url_queue.pop() #We grab a URL from the left of the list
            current_url = self.strip_url(current_url) #TODO generalnie ten krok wykonywany jest w funkcji get_links
            
            self.Crawlerlog.debug('test %s queue len: %s PageCrawlStart: %s',self.testname,len(self.url_queue),current_url)

            
            self.crawled_urls.append(current_url) #We then add this URL to our crawled list

            url_content_type, HTTPerror = self.url_http_status(current_url) #we check url to get basic info like content-type and status code this will allow us to exclude nonhtml and not working urls without even running selenium

            if HTTPerror is not None: #check if we have error during opening page if yes add this url to error list and skip rest of process for this url
                self.ApendErrorList(current_url,HTTPerror)
                continue

            if 'html' not in url_content_type: continue #if content not html based we can skip other steps

            
            html, CurrentResults = self.SeleniumLoadPage(current_url) #Now we do selenium part load page generate har/scrnshot and dictionary with results

            if html is not None: soup = self.get_soup(html) #check if page source was passed if not log error and skip other steps. if yes generate Boutifullsoup soup from page source
            else: 
                self.ApendErrorList(current_url,'CRITICAL ERROR: Page source was null')
                continue

            if soup is not None: #now if soup is not empty we can crawl to generate final results.
                CurrentResults = self.get_links(soup, CurrentResults)

            #self.results[current_url]=CurrentResults # add results from current page to our results dictionary
            self.results.append(CurrentResults)    #[current_url]=CurrentResults # add results from current page to our results dictionary
            
            
            
            PageElapsedTime = time.time() - PageElapsedTime
            self.Crawlerlog.debug('test %s url: %s crawled succesfully in %ss',self.testname,current_url,str(int(PageElapsedTime)))


        testelapsedtime = time.time()-testelapsedtime #TMP
        #now we can generate full report
        self.testsummary['CrawledPages']=self.results 
        self.testsummary['Errors']=self.url_error_list
        self.testsummary['crawledPages']=len(self.crawled_urls)
        self.testsummary['errors']=len(self.url_error_list)
        self.testsummary['timeelapsed']=int(testelapsedtime)

        try:
            #write report to json file
            jsonfile = self.ResultDir+'/results.json'
            f = open(jsonfile,'w')
            f.write(json.dumps(self.testsummary))
            f.close()

            HTMLReporting.GenerateReport(jsonfile,self.ResultDir)
        except Exception as e:
            self.Crawlerlog.critical('test %s encountered error while generating json/report file. error message: %s',self.testname,e)



        if self.Capturatrafic == True: self.server.stop() #if HAr was generated stop proxy
        self.browser.quit() #and stop browser


        self.Crawlerlog.info('test %s finished in %ss. Crawled %s pages. Queue have %s items. , results can be found at: %s',self.testname,str(int(testelapsedtime)),str(len(self.crawled_urls)),str(len(self.url_queue)),self.ResultDir)


