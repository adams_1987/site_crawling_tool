from pathlib import Path



class utils(object):
    """description of class"""


    def checkfileoverwrite(file):

        filecheck = None
        my_file = Path(file)

        if my_file.is_file():
            print(' file '+str(file)+' already exist overwrite? (yes):')
            q = input()
            if q == 'yes':
                filecheck=True
            else:
                filecheck=False
        else:
            filecheck=True
    
        return filecheck

    # When you need to ensure there are no empty elements in list
    def RmEmptStrFromList(list):
        while '' in list: list.remove('')
        return list