import logging

#CRITICAL	50
#ERROR	40
#WARNING	30
#INFO	20
#DEBUG	10
#NOTSET	0

def get_logger(name='__name__',loglvl='WARNING',logfilelocation='sample.log'):


    logger = logging.getLogger(name) #initialise logger

    #choose log lvl
    if loglvl=='CRITICAL': logger.setLevel(logging.CRITICAL)
    elif loglvl=='ERROR': logger.setLevel(logging.ERROR)
    elif loglvl=='WARNING': logger.setLevel(logging.WARNING)
    elif loglvl=='INFO': logger.setLevel(logging.INFO)
    elif loglvl=='DEBUG': logger.setLevel(logging.DEBUG)
    elif loglvl=='NOTSET': logger.setLevel(logging.NOTSET)
    else: logger.setLevel(logging.NOTSET)

    #set fromatter
    formatter = logging.Formatter('[%(levelname)s - %(asctime)s - %(name)s]: %(message)s')

    #setup file handling
    file_handler = logging.FileHandler(logfilelocation)
    file_handler.setFormatter(formatter)

    #setup console handling
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(formatter)

    logger.addHandler(file_handler) #init logging to file
    logger.addHandler(consoleHandler) #init logging to console

    return logger


