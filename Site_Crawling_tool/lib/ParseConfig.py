from configparser import ConfigParser
import argparse
import sys
#from lib.ParameterParser import ParameterParser
from pathlib import Path
from lib.utils import *

class CreateConfFragment(object):

    def ParseFullConfig(config,args,IsFullConfig=False):

        config = CreateConfFragment.Test_Config_section(config,args,IsFullConfig)
        config = CreateConfFragment.main_config_section(config,args,IsFullConfig)
        config = CreateConfFragment.proxy_config_section(config,args,IsFullConfig)
        config = CreateConfFragment.browser_config_section(config,args,IsFullConfig)
        
        return config

    def main_config_section(config,args,IsFullConfig):

        section_name='main_config'

        sectionDict ={
            #'setting debug lvl accepted values critical|error|warning|info|debug|notset':'#',
            'setting debug lvl accepted values critical|error|warning|info|debug|notset':'#',
            'loglvl' : 'WARNING',
            }

        config = CreateConfFragment.ParseConfigSection(section_name,sectionDict,config,args,IsFullConfig)

        return config

    def browser_config_section(config,args,IsFullConfig):

        section_name='browser_config'

        sectionDict ={
            'ff_geckolocation':'drivers\geckodriver.exe',
            'ch_exec':'drivers\chromedriver.exe',
            'browser_width':'1366',
            'browser_height':'768',
            }

        config = CreateConfFragment.ParseConfigSection(section_name,sectionDict,config,args,IsFullConfig)

        return config

    def proxy_config_section(config,args,IsFullConfig):

        section_name='proxy_config'

        sectionDict ={
            'proxy_dir' : 'c:\\browsermob-proxy\\bin\\browsermob-proxy',
            'proxy_port' : 1410,
            'CaptureHeader' : True,
            }

        config = CreateConfFragment.ParseConfigSection(section_name,sectionDict,config,args,IsFullConfig)

        return config

    def Test_Config_section(config,args,IsFullConfig):

        section_name='Test_Config'

        sectionDict = {
                'base_url' : '',
                #'choose browser type phantomjs|chrome|firefox default=phantomjs':'#',
                'choose browser type phantomjs|chrome|firefox default=phantomjs':'#',
                'TestBrowser':'phantomjs',
                #'decide if we want to capture traffic and screenshot accepted values True|False default valuse True':'#',
                'decide if we want to capture traffic and screenshot accepted values True|False default valuse True':'#',
                'Capturatrafic': True,
                'CaptureScreen': True,
                #'inclusion and exclusion list should be coma seperated list of url fragments':'#',
                'inclusion and exclusion list should be coma seperated list of url fragments':'#',
                'exclusion_list' : '',
                'inclusion_list' : '',
                }

        config = CreateConfFragment.ParseConfigSection(section_name,sectionDict,config,args,IsFullConfig)

        return config

    def ParseConfigSection(section_name,sectionDict,config,args,IsFullConfig):

        if config.has_section(section_name) is False: config.add_section(section_name) #if we don't have this section create it

        for k,v in dict(sectionDict).items():
            if v=='#' and IsFullConfig is True: #adding comment (only if we create full config)
                comment= '# '+ str(k)
                config.set(section_name,comment,'')
            elif k in args:
                config.set(section_name,str(k),str(args[k]))
                continue
            elif IsFullConfig is True:
                config.set(section_name,str(k),str(v))
        
        
        if len(config.items(section_name))==0:
            config.remove_section(section_name)

        return config


    def ClearSysArgDefaults(optionalargs):


        CmdArgs = vars(optionalargs)

        for k, v in dict(CmdArgs).items():
            if v is 'DefaultValue':
                del CmdArgs[k]


        return CmdArgs

class CreateConfigFile(object):

    #function designed to creat main program configuration
    def CreateMainConfig(optionalargs):




        args = CreateConfFragment.ClearSysArgDefaults(optionalargs) #create dict  from namespace passed from command line arguments

        #invoke configuration parser as config and add static config sections from given parameters
        config = ConfigParser() 


        config = CreateConfFragment.ParseFullConfig(config,args,IsFullConfig=True)

    
        filepath='main.conf' #main configuration have fixed filename

        filecheck = utils.checkfileoverwrite(filepath) #check if file already exists

        if filecheck == True: CreateConfigFile.WriteConfigToFile(filepath,config) #write config to file if we accepted overwrite or there is no file to overwrite
        else: print('file already exist skipping file write.')


        

    #function designed to create  configuration for single test
    def CreateTestConfig(optionalargs):

        args = CreateConfFragment.ClearSysArgDefaults(optionalargs) #create dict  from namespace passed from command line arguments removing ones with 'defaultvalue'

        config = ConfigParser() #invoke configuration parser as config and add static config sections from given parameters

        config = CreateConfFragment.ParseFullConfig(config,args,IsFullConfig=False)


        filepath='testcase/'+str(args.pop('configfilename','exampletest'))+'.conf' #test configuration filename combined from  given argument (must be before adding other config section because we need to remove filename first

        filecheck = utils.checkfileoverwrite(filepath) #check if file already exists
        if filecheck == True: CreateConfigFile.WriteConfigToFile(filepath,config) #write config to file if we accepted overwrite or there is no file to overwrite
        else: print('file already exist skipping file write.')



    #function designed to fix comment issue '# = ' when adding comments directly into config dictionary 
    def clearcomments(file):

        with open(file,'r') as t: filedata = t.read()
        filedata = filedata.replace('# =', '#')     
        with open(file,'w') as f: f.write(filedata)


    #simply write config to file :) 
    def WriteConfigToFile(filepath,config):
            with open(filepath,'w') as f: config.write(f)
            CreateConfigFile.clearcomments(filepath)
