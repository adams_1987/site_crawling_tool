from jinja2 import Environment, FileSystemLoader
import json
import os


class HTMLReporting():


    def ReportFromFolder(jsonfile):

        reportfolder = os.path.dirname(jsonfile)
        HTMLReporting.GenerateReport(jsonfile,reportfolder)


    def GenerateReport(jsonlocation,reportfolder):


        jsonfile = open(jsonlocation,'r')        
        dict = json.loads(jsonfile.read())
        jsonfile.close()

        file_loader = FileSystemLoader('templates')
        env = Environment(loader=file_loader)

        template = env.get_template('SummaryReport.html')
        output = template.render(data=dict)

        reportfile = reportfolder + '/results.html'

        f = open(reportfile,'w',encoding="utf-8")
        f.write(output)

        f.close()







if __name__ == "__main__":
    Reporting.GenerateReport()