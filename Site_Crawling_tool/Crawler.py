from configparser import ConfigParser
import argparse
from lib.ParseConfig import *
from lib.SeleniumCrawler import SeleniumCrawler
import sys
from lib.HTMLReporting import *
import re
from pathlib import Path


def MainConfig(optionalargs):

     CreateConfigFile.CreateMainConfig(optionalargs)


def TestConfig(optionalargs):

    CreateConfigFile.CreateTestConfig(optionalargs)


def StartCrawler(optionalargs):

    CMDargs = CreateConfFragment.ClearSysArgDefaults(optionalargs)

    config = ConfigParser()
    config.read('main.conf')

    
    if 'TestConfigName' in CMDargs:
        config.read(str(CMDargs['TestConfigName']))
        print('test config read')

    config = CreateConfFragment.ParseFullConfig(config,CMDargs,False)

    SeleniumCrawler(config)

def GenerateReport(optionalargs):

    s = optionalargs.ResultFile
    
    HTMLReporting.ReportFromFolder(s)


if __name__ == '__main__':

    #TMP for debug purpose
    #sys.argv = ['crawler.py', 
    #            'crawl', 
    #            '--loglvl=DEBUG', 
    #            #'--ResultFile=E:\\work_projects\\Python\\Site_Crawling_tool\\Site_Crawling_tool\\results\\com\\gene\\40dev\\102847131218\\results.json'
    #            #r'--ResultFile=E:\work_projects\Python\Site_Crawling_tool\Site_Crawling_tool\results\com\gene\40dev\102847131218\results.json'
    #            '--base_url=http://digital.gene.com/',
    #            #'--base_url=http://gwiz.gene.com/groups/Humres/tools/toolkit/',
    #            '--Capturatrafic=True', 
    #            '--CaptureScreen=True', 
    #            '--exclusion_list=/wp-content/,/wp-admin/,google.com,gwiz4,/wp-login.php,?regarding,.pdf,?replytocom', 
    #            #'--inclusion_list=gwizdev.gene.com/gredit/,gwizdev.gene.com/voiceit/',
    #            #'--inclusion_list=/cms/,/gwp/site/gwiz/',
    #            #'--inclusion_list=/groups/Humres/tools/toolkit/,gwiz.gene.com',
    #            '--testbrowser=firefox',
    #            #'GenerateReport',
    #            #r'--ResultFile=E:\work_projects\Python\Site_Crawling_tool\Site_Crawling_tool\results\com\gene\gwizdev\it\125134080119\results.json'
    #            ]
    #print(sys.argv)
    #############

    FUNCTION_MAP = {'crawl': StartCrawler,
                    'Configmain' : MainConfig,
                    'Configtest' : TestConfig,
                    'GenerateReport' : GenerateReport}
    
    parser = argparse.ArgumentParser(description='Welcome to magnificent Crawling tool :)')

    parser.add_argument('command', choices=FUNCTION_MAP.keys(), 
                        help='Choose what you want to do "crawl" runs crawler "ConfigMain" creates configuration file "ConfigTest" creates configuration for single test', default='')


    
    TestConfig = parser.add_argument_group('arguments for crawler', description='arguments used for crawler. we can pass all other arguments to override  config defaults/files')
    TestConfig.add_argument('--TestConfigName', required=False,
                            default='DefaultValue',
                            help='provide path to test configuration file')




    #optional arguments for Test config:
    TestConfigArgs = parser.add_argument_group('TestConfigArgs', description='optional arguments for Test config:')

    TestConfigArgs.add_argument('--configfilename', required=False , action='store', 
                                default='DefaultValue',
                                help='config creation only: define filename for test configuration')
    TestConfigArgs.add_argument('--base_url', required=False , action='store', 
                                default='DefaultValue',
                                help='define base url from witch we start crawling site')
    TestConfigArgs.add_argument('--testbrowser', required=False,
                            default='DefaultValue',
                            help='choose browser phantomjs|chrome|firefox default=phantomjs')
    TestConfigArgs.add_argument('--Capturatrafic', required=False , action='store', choices=['True','False'], 
                                default='DefaultValue',
                                help='define if we want to capture httptraffic')
    TestConfigArgs.add_argument('--CaptureHeader', required=False , action='store', choices=['True','False'], 
                                default='DefaultValue',
                                help='define if we want to capture httptraffic Headers')
    TestConfigArgs.add_argument('--CaptureScreen', required=False , action='store', choices=['True','False'], 
                                default='DefaultValue',
                                help='define if we want to capture screenshot')
    TestConfigArgs.add_argument('--exclusion_list', required=False , action='store', 
                                default='DefaultValue',
                                help='define url fragments you wish to exclude')
    TestConfigArgs.add_argument('--inclusion_list', required=False , action='store', 
                                default='DefaultValue',
                                help='define url fragments you wish to include')

    #optional arguments for Main config
    MainConfigArgs = parser.add_argument_group('MainConfigArgs', description='optional arguments for Main config:')
    MainConfigArgs.add_argument('--loglvl', required=False ,
                                default='DefaultValue', 
                                help='set log lvl', action='store',
                                choices=['CRITICAL','ERROR','WARNING','INFO','DEBUG','NOTSET'])
    MainConfigArgs.add_argument('--proxy_dir', required=False , action='store', 
                                default='DefaultValue',
                                help='define custom browsermobproxy location.')
    MainConfigArgs.add_argument('--proxy_port', required=False , action='store', 
                                default='DefaultValue',
                                help='define custom browsermobproxy port. value must be integer')
    MainConfigArgs.add_argument('--ff_geckolocation', required=False , action='store', 
                                default='DefaultValue',
                                help='define geckodriver location')
    MainConfigArgs.add_argument('--ch_exec', required=False , action='store', 
                                default='DefaultValue',
                                help='define chrome driver location')
    MainConfigArgs.add_argument('--browser_width', required=False , action='store', 
                                default='DefaultValue',
                                help='define browser height')
    MainConfigArgs.add_argument('--browser_height', required=False , action='store', 
                                default='DefaultValue',
                                help='define browser width')

    #optional arguments for report generator
    ReportGeneratorArgs = parser.add_argument_group('ReportGeneratorArgs', description='optional arguments for report generator')
    ReportGeneratorArgs.add_argument('--ResultFile', required=False,
                                     default='DefaultValue',
                                     help='provide jsonfile location')

    args = parser.parse_args()



    func = FUNCTION_MAP[args.command]
    func(args)
